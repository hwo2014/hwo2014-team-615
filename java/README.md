## Java bot template for HWO 2014

# Some code conventions and other stuff for team 3monkeys
* block indentation: 4 spaces (no tabs)
* if a parameter/variable/field can be **final**, make it **final**
* do test first
* only commit if tests are green
* if something is immutable, make it immutable
* try to use hamcrest in tests
* max line width should be 100
* use google guavas Objects.equals and Objects.hashCode for overriding equals and hashCode

# Installation

Enjoy Java8 and Maven!

Install (OS X):

Download and install [JDK8](https://jdk8.java.net/download.html).

    brew install maven
    export JAVA_HOME="`/usr/libexec/java_home -v '1.8*'`"

Install (Debian/Ubuntu):

    sudo apt-get install maven

Follow instructions for installing Java8 on [Debian](http://www.webupd8.org/2014/03/how-to-install-oracle-java-8-in-debian.html) or [Ubuntu](http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html).
