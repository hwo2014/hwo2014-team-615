package de.threemonkeys;

import de.threemonkeys.communication.HwoBotClient;
import de.threemonkeys.config.ApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Main {
    public static void main(final String... args) throws IOException {
        final String host = args[0];
        final int port = Integer.parseInt(args[1]);
        final String botName = args[2];
        final String botKey = args[3];

        try (final Socket socket = new Socket(host, port);
                final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
                final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"))) {

            ApplicationContext.init(writer, reader);
            final HwoBotClient client = ApplicationContext.getHwoBotClient();

            if (args.length == 5) {
                final String track = args[4];
                client.startGame(botName, botKey, track, 1);
            } else if (args.length == 7) {
                final String trackName = args[4];
                final String password = args[5];
                final int carCount = Integer.parseInt(args[6]);
                client.joinGame(botName, botKey, trackName, password, carCount);
            } else {
                client.startDefaultGame(botName, botKey);
            }
        }
    }
}
