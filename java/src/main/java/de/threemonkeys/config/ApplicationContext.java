package de.threemonkeys.config;

import de.threemonkeys.action.CarPositionsAction;
import de.threemonkeys.action.CrashAction;
import de.threemonkeys.action.GameInitAction;
import de.threemonkeys.action.GameStartAction;
import de.threemonkeys.action.IncomingMessageController;
import de.threemonkeys.action.JoinAction;
import de.threemonkeys.action.LapFinishedAction;
import de.threemonkeys.action.SpawnAction;
import de.threemonkeys.action.TurboReadyMessage;
import de.threemonkeys.action.YourCarAction;
import de.threemonkeys.communication.BotCommunicator;
import de.threemonkeys.communication.HwoBotClient;
import de.threemonkeys.communication.parser.IncomingMessageParser;
import de.threemonkeys.domain.Car;
import de.threemonkeys.domain.QueuedCarCommands;
import de.threemonkeys.domain.Track;
import de.threemonkeys.domain.physics.Physics;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class ApplicationContext {
    private static PrintWriter writer;
    private static BufferedReader reader;
    private static boolean init = false;
    private static HwoBotClient hwoBotClient;
    private static IncomingMessageController incomingMessageHandler;
    private static BotCommunicator botCommunicator;
    private static IncomingMessageParser incomingMessageParser;
    private static Car car;
    private static Track track;
    private static QueuedCarCommands carCommands;
	private static Physics physics;

    public static void init(final PrintWriter writer, final BufferedReader reader) {
        ApplicationContext.writer = writer;
        ApplicationContext.reader = reader;
        init = true;
    }

    public static HwoBotClient getHwoBotClient() {
        checkState();
        if (hwoBotClient == null) {
            hwoBotClient = new HwoBotClient(
                    getBotCommunicator(),
                    getIncomingMessageHandler()::handle,
                    getIncomingMessageParser());
        }
        return hwoBotClient;
    }

    private static IncomingMessageParser getIncomingMessageParser() {
        checkState();
        if (incomingMessageParser == null) {
            incomingMessageParser = new IncomingMessageParser();
        }
        return incomingMessageParser;
    }

    public static BotCommunicator getBotCommunicator() {
        checkState();
        if (botCommunicator == null) {
            botCommunicator = new BotCommunicator(reader, writer);
        }
        return botCommunicator;
    }

    public static IncomingMessageController getIncomingMessageHandler() {
        checkState();
        if (incomingMessageHandler == null) {
            incomingMessageHandler = new IncomingMessageController(getCarCommands());
            incomingMessageHandler.addAction(new CarPositionsAction(getCar(), getPhysics()));
            incomingMessageHandler.addAction(new GameInitAction(getCar(), getTrack(), getPhysics()));
            incomingMessageHandler.addAction(new JoinAction());
            incomingMessageHandler.addAction(new YourCarAction(getCar()));
            incomingMessageHandler.addAction(new TurboReadyMessage(getCar()));
            incomingMessageHandler.addAction(new CrashAction(getCar(), getCarCommands()));
            incomingMessageHandler.addAction(new SpawnAction(getCar()));
            incomingMessageHandler.addAction(new LapFinishedAction());
            incomingMessageHandler.addAction(new GameStartAction(getCar()));
        }
        return incomingMessageHandler;
    }

    private static Physics getPhysics() {
    	checkState();
    	if(physics == null) {
    		physics = new Physics();
    	}
    	return physics;
	}

	private static QueuedCarCommands getCarCommands() {
        checkState();
        if (carCommands == null) {
            carCommands = new QueuedCarCommands();
        }
        return carCommands;
    }

    public static Car getCar() {
        checkState();
        if (car == null) {
            car = new Car(getCarCommands(), getTrack(), getPhysics());
        }
        return car;
    }

    public static Track getTrack() {
        checkState();
        if (track == null) {
            track = new Track();
        }
        return track;
    }

    private static void checkState() {
        if (!init) {
            throw new RuntimeException("Application context was not initialized");
        }
    }
}
