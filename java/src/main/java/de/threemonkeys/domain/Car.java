package de.threemonkeys.domain;

import de.threemonkeys.communication.model.incoming.CarId;
import de.threemonkeys.domain.physics.PhysicsQuery;

public class Car {
    private final Track track;
    private final CarCommands commands;
    private final PhysicsQuery physics;
    private CarId id;

    private double maxForce = Double.MAX_VALUE;
    private double currentVelocity;
    private double currentAngle;
    private boolean hasTurbo;

    public Car(final CarCommands commands, final Track track, final PhysicsQuery physics) {
        this.commands = commands;
        this.track = track;
        this.physics = physics;
    }

    public void initCarId(final CarId id) {
        this.id = id;
    }

    public boolean isOwnCar(final CarId id) {
        return id.equals(id);
    }

    public void initPosition(final long gameTick) {
    }

    public void next(final long gameTick, final int pieceIndex, final double inPieceDistance, final double angle) {
        currentVelocity = physics.velocity(id);
        currentAngle = angle;

        // FIXME: find out when turbo can be used (Prio 1)
        // FIXME: find out when to switch lanes (Prio 1)
        final double bendVelocity = physics.maxSpeedForForce(maxForce, track.nextBend(pieceIndex));
        final double breakDistance = physics.ifBreak(currentVelocity, bendVelocity);
        final double throttle;
        if (track.isBendPieceNear(pieceIndex, inPieceDistance, breakDistance)) {
            // FIXME: perhaps we should use more than two values here? (Prio 2)
            if (currentVelocity > bendVelocity) {
                throttle = 0.0;
            } else {
                // FIXME: learn constant from track
                // (0.7 could be better in france?; Prio 1)
                throttle = 1.0;
            }
        } else {
            throttle = 1.0;
        }
        commands.throttle(throttle);

        if (physics.velocity(id) > 0) {
            System.out.println(String.format(
                    "T: %.5f\tV: %.5f\tF: %.5f\tMF: %.5e\tBV: %.5e\tBD: %.5f",
                    throttle, physics.velocity(id),
                    physics.centripedalForce(id), maxForce, bendVelocity, breakDistance));
        }
    }

    public void learnCrash(final CarId carId) {
        final double newForce = Math.min(physics.centripedalForce(carId) * 0.85, maxForce);
        System.out.println("Crash at force " + newForce + " -> new max " + maxForce);
        maxForce = newForce;
    }

    public void crash(final long gameTick) {
        hasTurbo = false;
        System.out.println("Crash at angle " + currentAngle + ", speed: " + currentVelocity + ", maxForce: " + maxForce);
    }

    public void turboReady(final int ticks, final double factor) {
        hasTurbo = true;
    }

    public void spawn(final long gameTick) {
        System.out.println("Spawned");
        commands.throttle(1.0);
    }

    public void startGame() {
    }
}
