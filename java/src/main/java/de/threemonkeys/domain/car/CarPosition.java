package de.threemonkeys.domain.car;

public class CarPosition {
    public final int trackPieceIndex;
    public final double inPieceDistance;
    public final long gameTick;

    public CarPosition(final long gameTick, final int trackPieceIndex,
            final double inPieceDistance) {
        this.gameTick = gameTick;
        this.trackPieceIndex = trackPieceIndex;
        this.inPieceDistance = inPieceDistance;
    }
}
