package de.threemonkeys.domain;

import de.threemonkeys.communication.model.outgoing.OutgoingMessage;
import de.threemonkeys.communication.model.outgoing.SwitchDirection;

public interface CarCommands {

    OutgoingMessage next();

    void throttle(double d);

    void switchLane(SwitchDirection direction);

    void turbo();

    void clear();

}
