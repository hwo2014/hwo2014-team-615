package de.threemonkeys.domain.physics;

import de.threemonkeys.communication.model.incoming.CarPosition;
import de.threemonkeys.communication.model.incoming.GameInit;

public interface PhysicsUpdate {
	void update(CarPosition[] positions);
	void init(GameInit content);
}
