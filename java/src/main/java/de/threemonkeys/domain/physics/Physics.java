package de.threemonkeys.domain.physics;

import de.threemonkeys.communication.model.incoming.Car;
import de.threemonkeys.communication.model.incoming.CarId;
import de.threemonkeys.communication.model.incoming.CarPosition;
import de.threemonkeys.communication.model.incoming.GameInit;
import de.threemonkeys.communication.model.incoming.Lane;
import de.threemonkeys.communication.model.incoming.Piece;
import de.threemonkeys.communication.model.incoming.primitives.PiecePosition;

import java.util.HashMap;
import java.util.Map;

public class Physics implements PhysicsUpdate, PhysicsQuery {

    private final Map<CarId, PhysicalProperties> cars = new HashMap<CarId, PhysicalProperties>();
    private CarPosition[] lastPositions;

    private Piece[] trackPieces;
    // FIXME: merge with Track class (Prio 3)
    private Lane[] lanes;

    @Override
    public void update(final CarPosition[] positions) {

        if(lastPositions == null) {
            lastPositions = positions;
            return;
        }

        for (final CarId carId : cars.keySet()) {
            final CarPosition oldPosition = GetPosition(carId, lastPositions);
            CarPosition newPosition = GetPosition(carId, positions);

            for (final CarPosition carPosition : positions) {
                if(carPosition.getId().equals(carId)) {
                    newPosition = carPosition;
                }
            }

            if (oldPosition.getPiecePosition().getInPieceDistance() != newPosition.getPiecePosition().getInPieceDistance() ||
                    oldPosition.getPiecePosition().getPieceIndex() != newPosition.getPiecePosition().getInPieceDistance()) {
                updateProperties(oldPosition, newPosition, cars.get(carId));
            }
        }

        lastPositions = positions;
    }

    private CarPosition GetPosition(final CarId carId, final CarPosition[] positions) {
        for (final CarPosition carPosition : positions) {
            if(carPosition.getId().equals(carId)) {
                return carPosition;
            }
        }

        return null;
    }

    private void updateProperties(final CarPosition oldPosition, final CarPosition newPosition, final PhysicalProperties physicalProperties) {
        physicalProperties.setVelocity(getVelocity(oldPosition.getPiecePosition(), newPosition.getPiecePosition()));
        physicalProperties.setInTrackPosition(getInTrackPosition(newPosition.getPiecePosition()));
        final double force = getCentrifugalForce(oldPosition.getPiecePosition(), newPosition.getPiecePosition());
        if (force != 0.0) {
            physicalProperties.setCentrifugalForce(force);
        }
    }

    private double getCentrifugalForce(final PiecePosition oldPiecePosition, final PiecePosition piecePosition) {
        final Piece trackPiece = trackPieces[piecePosition.getPieceIndex()];

        final boolean isBend = trackPiece.getAngle() != 0.0;
        if (!isBend || oldPiecePosition.getPieceIndex() != piecePosition.getPieceIndex()) {
            return 0;
        }

        final double distance = piecePosition.getInPieceDistance() - oldPiecePosition.getInPieceDistance();
        final double omega = distance / trackPiece.getRadius();

        return omega * omega * trackPiece.getRadius();
    }

    private double getInTrackPosition(final PiecePosition piecePosition) {
        double position = 0.0;
        for(int pieceIndex = 0; pieceIndex < piecePosition.getPieceIndex(); pieceIndex++) {
            position += getLength(lanes[piecePosition.getLane().getEndLaneIndex()], trackPieces[pieceIndex]);
        }
        return position + piecePosition.getInPieceDistance();
    }

    private double getVelocity(final PiecePosition oldPosition, final PiecePosition newPosition) {
        final Piece lastPiece = trackPieces[oldPosition.getPieceIndex()];
        final Piece currentPiece = trackPieces[newPosition.getPieceIndex()];

        double velocity;
        if(lastPiece == currentPiece) {
            velocity = newPosition.getInPieceDistance() - oldPosition.getInPieceDistance();
        } else {
            final double lastPieceDistance = getLength(lanes[oldPosition.getLane().getEndLaneIndex()], lastPiece) - oldPosition.getInPieceDistance();
            velocity = newPosition.getInPieceDistance() + lastPieceDistance;
        }

        return velocity;
    }

    private double getLength(final Lane lane, final Piece piece) {
        if(piece.getLength() > 0) {
            return piece.getLength();
        }

        double laneRadius = piece.getRadius();
        if(piece.getAngle() > 0) {
            // right turn
            laneRadius -= lane.getDistanceFromCenter();
        } else {
            // left turn
            laneRadius += lane.getDistanceFromCenter();
        }

        return (Math.abs(piece.getAngle()) / 360.0) * ( laneRadius * Math.PI  * 2.0);
    }

    @Override
    public void init(final GameInit game) {
        trackPieces = game.getRace().getTrack().getPieces();
        lanes = game.getRace().getTrack().getLanes();

        for (final Car car : game.getRace().getCars()) {
            cars.put(car.getId(), new PhysicalProperties( car.getDimensions() ));
        }
    }

    @Override
    public double velocity(final CarId id) {
        return cars.get(id).getVelocity();
    }

    @Override
    public double ifBreak(final double vStart, final double vTarget) {
        int gameTick = 0;

        final double drag = 0.02;

        final double fixedTarget = Math.max(vTarget, 1);
        double distance = 0;
        double estimatedVelocity;
        do {
            estimatedVelocity = vStart * Math.exp(-drag * gameTick) + (0.01 / drag) * (1-Math.exp(-drag * gameTick));
            //System.out.println(estimatedVelocity);
            distance += estimatedVelocity;
            gameTick++;
        } while (estimatedVelocity > fixedTarget);

        return distance;
    }

    @Override
    public double inTrackPosition(final CarId id) {
        return cars.get(id).getInTrackPosition();
    }

    @Override
    public double centripedalForce(final CarId id) {
        return cars.get(id).getCentrifugalForce();
    }

    @Override
    public double maxSpeedForForce(final double force, final int pieceIndex) {
        final Piece trackPiece = trackPieces[pieceIndex];
        final double omega = Math.sqrt(force / trackPiece.getRadius());
        return omega * trackPiece.getRadius();
    }
}
