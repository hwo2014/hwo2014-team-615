package de.threemonkeys.domain.physics;

import de.threemonkeys.communication.model.incoming.CarId;

public interface PhysicsQuery {
    double velocity(CarId id);
    double inTrackPosition(CarId id);
    double ifBreak(double vStart, double vTarget);
    double centripedalForce(CarId id);
    double maxSpeedForForce(double force, int pieceIndex);
}
