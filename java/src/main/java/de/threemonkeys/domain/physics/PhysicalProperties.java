package de.threemonkeys.domain.physics;

import de.threemonkeys.communication.model.incoming.CarDimensions;

public class PhysicalProperties {

    private final CarDimensions dimensions;
    private double velocity;
    private double inTrackPosition;
    private double centrifugalForce;

    public PhysicalProperties(final CarDimensions dimensions) {
        this.dimensions = dimensions;
    }

    public CarDimensions getDimensions() {
        return dimensions;
    }

    public void setVelocity(final double velocity) {
        this.velocity = velocity;
    }

    public double getVelocity() {
        return velocity;
    }

    public double getInTrackPosition() {
        return inTrackPosition;
    }

    public void setInTrackPosition(final double inTrackPosition) {
        this.inTrackPosition = inTrackPosition;
    }

    public void setCentrifugalForce(final double centrifugalForce) {
        this.centrifugalForce = centrifugalForce;
    }

    public double getCentrifugalForce() {
        return centrifugalForce;
    }

}
