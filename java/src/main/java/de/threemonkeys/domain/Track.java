package de.threemonkeys.domain;

import de.threemonkeys.communication.model.incoming.Piece;
import de.threemonkeys.domain.track.BendPiece;
import de.threemonkeys.domain.track.StaightPiece;
import de.threemonkeys.domain.track.TrackPiece;

import java.util.ArrayList;
import java.util.List;

public class Track {
    private final List<TrackPiece> pieces = new ArrayList<>();

    public void learn(
            final de.threemonkeys.communication.model.incoming.Track track) {
        pieces.clear();
        for (final Piece piece : track.getPieces()) {
            if (piece.getRadius() != 0f) {
                learnBend(piece);
            } else {
                learnStraight(piece);
            }
        }
    }

    private void learnBend(final Piece piece) {
        pieces.add(new BendPiece(piece.getRadius(), piece.getAngle()));
    }

    private void learnStraight(final Piece piece) {
        pieces.add(new StaightPiece(piece.getLength()));
    }

    public double getDistance(final int oldPiece, final double positionInOldPiece,
            final int newPiece, final double positionInNewPiece) {
        if (oldPiece == newPiece) {
            return positionInNewPiece - positionInOldPiece;
        } else {
            return pieces.get(oldPiece).getDistanceToEnd(positionInOldPiece) + positionInNewPiece;
        }
    }

    public boolean isBendPieceNear(final int piece, final double positionInPiece, final double distance) {
        int currentPieceId = piece;
        double currentDistance = distance;
        TrackPiece currentPiece = pieces.get(currentPieceId);

        while (currentDistance > 0) {
            if (currentPiece.isBendable()) {
                return true;
            } else {
                if (piece == currentPieceId) {
                    currentDistance -= currentPiece.getDistanceToEnd(positionInPiece);
                } else {
                    currentDistance -= currentPiece.length();
                }

                currentPieceId = (currentPieceId + 1) % pieces.size();
                currentPiece = pieces.get(currentPieceId);
            }
        }

        return currentPiece.isBendable();
    }

    public int nextBend(final int pieceIndex) {
        int currentPieceId = pieceIndex;
        TrackPiece piece = pieces.get(pieceIndex);
        while (!(piece instanceof BendPiece)) {
            currentPieceId = (currentPieceId + 1) % pieces.size();
            piece = pieces.get(currentPieceId);
        }
        return currentPieceId;
    }

}
