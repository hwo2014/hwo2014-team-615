package de.threemonkeys.domain.track;

public class StaightPiece implements TrackPiece {
    private final double length;

    public StaightPiece(final double length) {
        this.length = length;
    }

    @Override
    public double getDistanceToEnd(final double positionInOldPiece) {
        return length - positionInOldPiece;
    }

    @Override
    public boolean isBendable() {
        return false;
    }

    @Override
    public double length() {
        return length;
    }

}
