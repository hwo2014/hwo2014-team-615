package de.threemonkeys.domain.track;

public class BendPiece implements TrackPiece {
    private final double length;

    public BendPiece(final double radius, final double angle) {
        length = radius * angle / 180 * Math.PI;
    }

    @Override
    public double getDistanceToEnd(final double positionInOldPiece) {
        return length - positionInOldPiece;
    }

    @Override
    public boolean isBendable() {
        return true;
    }

    @Override
    public double length() {
        return length;
    }
}
