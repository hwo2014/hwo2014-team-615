package de.threemonkeys.domain.track;

public interface TrackPiece {

    double getDistanceToEnd(double positionInOldPiece);

    boolean isBendable();

    double length();

}
