package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;

public class JoinAction implements IncomingMessageAction {
    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        System.out.println("> Adaptive BOT: joined");
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.join;
    }
}
