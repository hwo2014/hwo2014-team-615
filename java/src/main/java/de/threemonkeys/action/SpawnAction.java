package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.CarId;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.domain.Car;

public class SpawnAction implements IncomingMessageAction {

    private final Car car;

    public SpawnAction(final Car car) {
        this.car = car;
    }

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        @SuppressWarnings("unchecked")
        final
        IncomingMessage<CarId> message = (IncomingMessage<CarId>) incomingMessage;

        if (car.isOwnCar(message.getContent())) {
            car.spawn(incomingMessage.getGameTick());
        }
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.spawn;
    }

}
