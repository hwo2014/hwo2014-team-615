package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.domain.Car;

public class GameStartAction implements IncomingMessageAction {

	private Car car;

	public GameStartAction(Car car) {
		this.car = car;
	}

	@Override
	public void handle(IncomingMessage<?> incomingMessage) {
		car.startGame();
	}

	@Override
	public MessageType getMessageType() {
		return MessageType.gameStart;
	}

}
