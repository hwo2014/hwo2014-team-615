package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.communication.model.outgoing.OutgoingMessage;
import de.threemonkeys.domain.QueuedCarCommands;

import java.util.HashMap;
import java.util.Map;

public class IncomingMessageController {
    private final Map<MessageType, IncomingMessageAction> actionsForMessageTypes = new HashMap<>();
    private final QueuedCarCommands commands;

    public IncomingMessageController(final QueuedCarCommands commands) {
        this.commands = commands;
    }

    public OutgoingMessage handle(final IncomingMessage<?> incomingMessage) {
        final MessageType messageType = incomingMessage.getMessageType();
        // System.out.println("> Get message type: " + messageType);
        if (actionsForMessageTypes.containsKey(messageType)) {
            actionsForMessageTypes.get(messageType).handle(incomingMessage);
        } else {
            System.out.println("**> WARNING: Unknown message: "
                    + incomingMessage.getMessageType());
        }
        return commands.next();
    }

    public void addAction(final IncomingMessageAction incomingMessageAction) {
        actionsForMessageTypes.put(incomingMessageAction.getMessageType(), incomingMessageAction);
    }
}
