package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.communication.model.incoming.TurboAvailable;
import de.threemonkeys.domain.Car;

public class TurboReadyMessage implements IncomingMessageAction {
    private final Car car;

    public TurboReadyMessage(final Car car) {
        this.car = car;
    }

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        @SuppressWarnings("unchecked")
        final IncomingMessage<TurboAvailable> message = ((IncomingMessage<TurboAvailable>) incomingMessage);
        car.turboReady(message.getContent().getTurboDurationTicks(),
                message.getContent().getTurboFactor());
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.turboAvailable;
    }

}
