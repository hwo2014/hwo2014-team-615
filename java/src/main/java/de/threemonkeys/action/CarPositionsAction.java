package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.CarPosition;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.domain.Car;
import de.threemonkeys.domain.physics.Physics;

public class CarPositionsAction implements IncomingMessageAction {
    private final Car car;
    private final Physics physics;
    
    public CarPositionsAction(final Car car, final Physics physics) {
        this.car = car;
        this.physics = physics;
    }

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        CarPosition[] positions = (CarPosition[]) incomingMessage.getContent();
        
        physics.update(positions);
        
		for (final CarPosition position : positions) {
            if (car.isOwnCar(position.getId())) {
                car.next(incomingMessage.getGameTick(), position.getPiecePosition().getPieceIndex(),
                        position.getPiecePosition().getInPieceDistance(),
                        position.getAngle());
                return;
            }
        }
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.carPositions;
    }
}
