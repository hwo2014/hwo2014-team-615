package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.CarId;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.domain.Car;
import de.threemonkeys.domain.CarCommands;

public class CrashAction implements IncomingMessageAction {
    private final Car car;
    private final CarCommands carCommands;

    public CrashAction(final Car car, final CarCommands carCommands) {
        this.car = car;
        this.carCommands = carCommands;
    }

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        @SuppressWarnings("unchecked")
        final IncomingMessage<CarId> message = (IncomingMessage<CarId>) incomingMessage;

        car.learnCrash(message.getContent());
        if (car.isOwnCar(message.getContent())) {
            carCommands.clear();
            car.crash(message.getGameTick());
        }
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.crash;
    }

}
