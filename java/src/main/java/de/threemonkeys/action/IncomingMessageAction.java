package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;

public interface IncomingMessageAction {
    void handle(IncomingMessage<?> incomingMessage);

    MessageType getMessageType();
}
