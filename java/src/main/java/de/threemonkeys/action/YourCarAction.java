package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.CarId;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.domain.Car;

public class YourCarAction implements IncomingMessageAction {
    private final Car car;

    public YourCarAction(final Car car) {
        this.car = car;
    }

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        car.initCarId((CarId) incomingMessage.getContent());
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.yourCar;
    }
}
