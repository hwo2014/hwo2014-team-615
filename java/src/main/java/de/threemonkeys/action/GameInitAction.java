package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.GameInit;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.domain.Car;
import de.threemonkeys.domain.Track;
import de.threemonkeys.domain.physics.Physics;

public class GameInitAction implements IncomingMessageAction {
    private final Car car;
    private final Track track;
	private Physics physics;

    public GameInitAction(final Car car, final Track track, final Physics physics) {
        this.car = car;
        this.track = track;
        this.physics = physics;
    }

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        System.out.println("> Adaptive BOT: initializing game...");

        GameInit content = (GameInit) incomingMessage.getContent();
        
        physics.init(content);
        
		track.learn(content.getRace().getTrack());
        car.initPosition(incomingMessage.getGameTick());

        System.out.println("> Adaptive BOT: initializing game... finished");
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.gameInit;
    }
}
