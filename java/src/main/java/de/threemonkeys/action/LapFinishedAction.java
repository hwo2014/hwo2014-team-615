package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.communication.model.incoming.LapFinished;

public class LapFinishedAction implements IncomingMessageAction {

    @Override
    public void handle(final IncomingMessage<?> incomingMessage) {
        @SuppressWarnings("unchecked")
        final
        IncomingMessage<LapFinished> message = (IncomingMessage<LapFinished>) incomingMessage;

        System.out.println("> *** Finished Round ***");
        System.out.println("Lap: " + message.getContent().getRaceTime().getLap());
        System.out.println("Ranking: " + message.getContent().getRanking().getRanking());
        System.out.println("Ranking (Overall): " + message.getContent().getRanking().getOverall());
        System.out.println("Race time (ms): " + message.getContent().getRaceTime().getMillis());
        System.out.println("Race time (ticks): " + message.getContent().getRaceTime().getTicks());
        System.out.println("> ***                ***");
    }

    @Override
    public MessageType getMessageType() {
        return MessageType.lapFinished;
    }

}
