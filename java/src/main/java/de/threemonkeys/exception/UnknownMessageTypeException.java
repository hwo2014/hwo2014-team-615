package de.threemonkeys.exception;

import de.threemonkeys.communication.model.MessageType;

public class UnknownMessageTypeException extends RuntimeException {
    private static final long serialVersionUID = -1641138978653316619L;

    public UnknownMessageTypeException(final String message, final MessageType msgType) {
        super("Message type " + msgType + " is unknown. Message content: " + message);
    }
}
