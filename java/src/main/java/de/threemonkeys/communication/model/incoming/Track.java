package de.threemonkeys.communication.model.incoming;

public class Track {
    private String id;
    private String name;
    private Piece[] pieces;
    private Lane[] lanes;
    private StartingPoint startingPoint;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Piece[] getPieces() {
        return pieces;
    }

    public Lane[] getLanes() {
        return lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }
}
