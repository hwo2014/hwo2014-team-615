package de.threemonkeys.communication.model.incoming;

public class TurboAvailable {
	private double turboDurationMilliseconds;
	private int turboDurationTicks;
	private double turboFactor;
	
	public double getTurboDurationMilliseconds() {
		return turboDurationMilliseconds;
	}
	
	public int getTurboDurationTicks() {
		return turboDurationTicks;
	}
	
	public double getTurboFactor() {
		return turboFactor;
	}
}
