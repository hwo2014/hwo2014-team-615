package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

public class Ping extends OutgoingMessage {
    public Ping() {
        super(MessageType.ping);
    }
}