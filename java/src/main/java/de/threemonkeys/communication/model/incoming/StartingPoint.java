package de.threemonkeys.communication.model.incoming;

public class StartingPoint {
    private Position position;
    private double angle;

    public Position getPosition() {
        return position;
    }

    public double getAngle() {
        return angle;
    }
}
