package de.threemonkeys.communication.model.outgoing;

public class BotId {
	public final String key;
	public final String name;
	
	public BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}
