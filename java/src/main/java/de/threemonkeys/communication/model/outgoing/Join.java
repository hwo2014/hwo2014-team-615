package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

public class Join extends OutgoingMessage {
    public Join(final String name, final String key) {
        super(MessageType.join, new BotId(name, key));
    }
}