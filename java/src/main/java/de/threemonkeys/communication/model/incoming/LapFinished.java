package de.threemonkeys.communication.model.incoming;

import de.threemonkeys.communication.model.incoming.primitives.GameTime;
import de.threemonkeys.communication.model.incoming.primitives.Ranking;

public class LapFinished {
	private Ranking ranking;
	private GameTime raceTime;
	private GameTime lapTime;
	private CarId car;
	
	public Ranking getRanking() {
		return ranking;
	}
	
	public GameTime getRaceTime() {
		return raceTime;
	}
	
	public GameTime getLapTime() {
		return lapTime;
	}
	
	public CarId getCar() {
		return car;
	}
}