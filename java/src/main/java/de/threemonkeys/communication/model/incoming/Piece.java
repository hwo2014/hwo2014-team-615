package de.threemonkeys.communication.model.incoming;

import com.google.gson.annotations.SerializedName;

public class Piece {
    private double length;

    @SerializedName("switch")
    private boolean isSwitch;

    private double radius;
    private double angle;

    public double getLength() {
        return length;
    }

    public boolean isSwitch() {
        return isSwitch;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }
}
