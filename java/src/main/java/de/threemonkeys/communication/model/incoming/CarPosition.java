package de.threemonkeys.communication.model.incoming;

import de.threemonkeys.communication.model.incoming.primitives.PiecePosition;

public class CarPosition {
    private CarId id;
    private double angle;
    private PiecePosition piecePosition;

    public double getAngle() {
        return angle;
    }

    public CarId getId() {
        return id;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }
}
