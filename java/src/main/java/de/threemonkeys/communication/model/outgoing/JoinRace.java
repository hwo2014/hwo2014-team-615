package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

public class JoinRace extends OutgoingMessage {
	public JoinRace(final String botName, final String botKey, final String trackName, final String password, final int carCount) {
		super(MessageType.joinRace, new RaceData(botName, botKey, trackName, password, carCount));
	}
}
