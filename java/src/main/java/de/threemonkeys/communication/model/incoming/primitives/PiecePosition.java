package de.threemonkeys.communication.model.incoming.primitives;

public class PiecePosition {
    private int pieceIndex;
    private double inPieceDistance;
    private Lane lane;

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public Lane getLane() {
        return lane;
    }
}