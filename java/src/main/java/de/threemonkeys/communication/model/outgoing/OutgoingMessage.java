package de.threemonkeys.communication.model.outgoing;

import com.google.gson.Gson;
import de.threemonkeys.communication.model.MessageType;

public abstract class OutgoingMessage {
    private static final Gson GSON = new Gson();

    private final MessageType messageType;
    private final Object messageData;

    protected OutgoingMessage(final MessageType messageType, final Object messageData) {
        this.messageType = messageType;
        this.messageData = messageData;
    }

    protected OutgoingMessage(final MessageType messageType) {
        this(messageType, null);
    }

    public final MessageType msgType() {
        return messageType;
    }

    public final String toJson() {
        return GSON.toJson(new SendableMessage(this));
    }

    public final Object msgData() {
        return messageData;
    }
}