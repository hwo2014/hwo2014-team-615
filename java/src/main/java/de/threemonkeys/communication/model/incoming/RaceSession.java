package de.threemonkeys.communication.model.incoming;

public class RaceSession {
    private int laps;
    private int maxLapTimeMs;
    private boolean quickRace;

    public int getLaps() {
        return laps;
    }

    public int getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }
}
