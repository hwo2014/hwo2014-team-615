package de.threemonkeys.communication.model.incoming;

public class Position {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
