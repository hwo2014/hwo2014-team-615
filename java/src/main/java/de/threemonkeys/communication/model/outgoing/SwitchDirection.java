package de.threemonkeys.communication.model.outgoing;

public enum SwitchDirection {
	// Due to server protocol switch directions must start with capital letters 
	Left,
	Right
}
