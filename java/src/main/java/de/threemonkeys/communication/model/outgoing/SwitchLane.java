package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

public class SwitchLane extends OutgoingMessage {
	public SwitchLane(final SwitchDirection direction) {
		super(MessageType.switchLane, direction);
	}
}
