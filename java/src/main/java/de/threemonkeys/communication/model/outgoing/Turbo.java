package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

public class Turbo extends OutgoingMessage {
	
	public Turbo() {
		this("Activating Turbo");
	}

	public Turbo(final String turboMessage) {
		super(MessageType.turbo, turboMessage);
	}
	
}
