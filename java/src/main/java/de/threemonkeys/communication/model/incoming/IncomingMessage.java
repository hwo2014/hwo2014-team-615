package de.threemonkeys.communication.model.incoming;

import de.threemonkeys.communication.model.MessageType;

public class IncomingMessage<T> {
    private final MessageType msgType;
    private final T data;
    private final String gameId;
    private final long gameTick;

    public IncomingMessage(final MessageType msgType, final T data, final String gameId, final long gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public MessageType getMessageType() {
        return msgType;
    }

    public T getContent() {
        return data;
    }

    public String getGameId() {
        return gameId;
    }

    public long getGameTick() {
        return gameTick;
    }
}
