package de.threemonkeys.communication.model;

public enum MessageType {
    join,
    carPositions,
    gameInit,
    yourCar,
    gameStart,
    lapFinished,
    gameEnd,
    tournamentEnd,
    spawn,
    crash,
    finish,
    ping,
    throttle,
    switchLane,
    dnf,
    turboAvailable,
    turbo,
    createRace,
    joinRace
}