package de.threemonkeys.communication.model.outgoing;


import de.threemonkeys.communication.model.MessageType;

public class Throttle extends OutgoingMessage {
    public Throttle(final double value) {
        super(MessageType.throttle, Math.min(1.0, Math.max(0.05, value)));
    }
}