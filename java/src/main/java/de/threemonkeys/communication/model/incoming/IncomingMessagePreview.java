package de.threemonkeys.communication.model.incoming;

import de.threemonkeys.communication.model.MessageType;

public class IncomingMessagePreview {
    private MessageType msgType;
    private String gameId;
    private Long gameTick;

    public IncomingMessagePreview() {
    }

    public MessageType getMsgType() {
        return msgType;
    }

    public String getGameId() {
        return gameId;
    }

    public Long getGameTick() {
        return gameTick;
    }
}
