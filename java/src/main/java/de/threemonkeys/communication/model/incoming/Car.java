package de.threemonkeys.communication.model.incoming;

public class Car {
    private CarDimensions dimensions;
    private CarId id;

    public CarDimensions getDimensions() {
        return dimensions;
    }

    public CarId getId() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();

        result.append(String.format("Id: %s\n", id));
        result.append(String.format("Dimensions: %s\n", dimensions));

        return result.toString();
    }
}
