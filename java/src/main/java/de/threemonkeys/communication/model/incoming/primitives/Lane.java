package de.threemonkeys.communication.model.incoming.primitives;

public class Lane {
    private int startLaneIndex;
    private int endLaneIndex;

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }
}