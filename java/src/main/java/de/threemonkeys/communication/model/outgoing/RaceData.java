package de.threemonkeys.communication.model.outgoing;

public class RaceData {
	public final BotId botId;
	
	public final String trackName;
	public final String password;
	public final int carCount;
	
	public RaceData(String botName, String botKey, String trackName, String password, int carCount) {
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
		this.botId = new BotId(botName, botKey);
	}
}
