package de.threemonkeys.communication.model.incoming;

public class Lane {
    private double distanceFromCenter;
    private int index;

    public double getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }
}
