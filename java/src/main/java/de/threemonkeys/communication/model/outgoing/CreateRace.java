package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

public class CreateRace extends OutgoingMessage {
	public CreateRace(final String botName, final String botKey, final String trackName, final String password, final int carCount) {
		super(MessageType.createRace, new RaceData(botName, botKey, trackName, password, carCount));
	}
}
