package de.threemonkeys.communication.model.incoming.primitives;

public class Ranking {
	private int overall;
	private int ranking;
	
	public int getOverall() {
		return overall;
	}
	
	public int getRanking() {
		return ranking;
	}
}