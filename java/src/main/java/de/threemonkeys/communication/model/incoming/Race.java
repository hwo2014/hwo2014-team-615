package de.threemonkeys.communication.model.incoming;


public class Race {
    private Track track;
    private Car[] cars;
    private RaceSession raceSession;

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public Car[] getCars() {
        return cars;
    }

    public Track getTrack() {
        return track;
    }
}
