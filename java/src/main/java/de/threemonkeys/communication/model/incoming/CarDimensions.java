package de.threemonkeys.communication.model.incoming;

public class CarDimensions {
    private double length;
    private double width;
    private double guideFlagPosition;

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }
}
