package de.threemonkeys.communication.model.incoming;

public class CarId {
    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
    
    @Override
    public int hashCode() {
    	return com.google.common.base.Objects.hashCode(name, color);
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null)  
        {  
           return false;  
        }  
        
        if (getClass() != other.getClass())  
        {  
           return false;  
        }
        
        final CarId carId = (CarId) other;
        
        return carId.getName().equals(name) && carId.getColor().equals(color);
    }
}
