package de.threemonkeys.communication.model.incoming;

public class GameInit {
	private Race race;

	public Race getRace() {
		return race;
	}
	
	@Override
	public String toString() {
		return race.toString();
	}
}
