package de.threemonkeys.communication.model.incoming.primitives;

public class GameTime {
	private int lap;
	private int ticks;
	private int millis;
	
	public int getLap() {
		return lap;
	}
	
	public int getTicks() {
		return ticks;
	}
	
	public int getMillis() {
		return millis;
	}
}