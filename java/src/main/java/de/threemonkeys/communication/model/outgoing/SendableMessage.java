package de.threemonkeys.communication.model.outgoing;

import de.threemonkeys.communication.model.MessageType;

class SendableMessage {
    public final MessageType msgType;
    public final Object data;

    public SendableMessage(final OutgoingMessage sendMsg) {
        this.msgType = sendMsg.msgType();
        this.data = sendMsg.msgData();
    }
}