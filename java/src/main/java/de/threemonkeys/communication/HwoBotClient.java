package de.threemonkeys.communication;

import com.google.gson.JsonSyntaxException;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.communication.model.outgoing.CreateRace;
import de.threemonkeys.communication.model.outgoing.Join;
import de.threemonkeys.communication.model.outgoing.JoinRace;
import de.threemonkeys.communication.model.outgoing.OutgoingMessage;
import de.threemonkeys.communication.parser.IncomingMessageParser;

import java.io.IOException;
import java.util.function.Function;

public class HwoBotClient {
    private final BotCommunicator botCommunicator;
    private final Function<IncomingMessage<?>, OutgoingMessage> messageHandler;
    private final IncomingMessageParser incomingMessageParser;

    public HwoBotClient(final BotCommunicator botCommunicator,
            final Function<IncomingMessage<?>, OutgoingMessage> messageHandler,
            final IncomingMessageParser incomingMessageParser) {
        this.botCommunicator = botCommunicator;
        this.messageHandler = messageHandler;
        this.incomingMessageParser = incomingMessageParser;
    }

    public void startDefaultGame(final String botName, final String botKey) throws JsonSyntaxException, IOException {
        botCommunicator.sendMessage(new Join(botName, botKey).toJson());
        communicationLoop();
    }

    public void startGame(final String botName, final String botKey, final String track, final int carCount) throws IOException {
        botCommunicator.sendMessage(new CreateRace(botName, botKey, track, botKey, carCount).toJson());
        communicationLoop();
    }

    public void joinGame(final String botName, final String botKey, final String trackName, final String password, final int carCount)
            throws JsonSyntaxException, IOException{
        botCommunicator.sendMessage(new JoinRace(botName, botKey, trackName, password, carCount).toJson());
        communicationLoop();
    }

    private void communicationLoop() throws IOException {
        String line;
        while ((line = botCommunicator.readNextMessage()) != null) {
            processLine(line);
        }
    }

    private void processLine(final String line) {
        final IncomingMessage<?> message = incomingMessageParser.parse(line);
        final OutgoingMessage responseMessage = messageHandler.apply(message);

        if (responseMessage != null) {
            botCommunicator.sendMessage(responseMessage.toJson());
        }
    }
}
