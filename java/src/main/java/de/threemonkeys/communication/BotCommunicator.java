package de.threemonkeys.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

public class BotCommunicator {
    private final BufferedReader reader;
    private final PrintWriter writer;

    public BotCommunicator(final BufferedReader reader, final PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    public void sendMessage(final String msg) {
        // System.out.println("S: " + msg);
        writer.println(msg);
        writer.flush();
    }

    public String readNextMessage() throws IOException {
        final String line = reader.readLine();
        // System.out.println("R: " + line);
        return line;
    }
}
