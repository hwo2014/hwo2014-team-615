package de.threemonkeys.communication.parser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.*;
import de.threemonkeys.communication.model.outgoing.BotId;
import de.threemonkeys.communication.model.outgoing.RaceData;
import de.threemonkeys.exception.UnknownMessageTypeException;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IncomingMessageParser {
    private final Gson gson = new Gson();
    private final Map<MessageType, Type> msgTypeToObjectRepresentation;

    {
        final Map<MessageType, Type> map = new HashMap<>();
        map.put(MessageType.join, new TypeToken<IncomingMessage<BotId>>() {}.getType());
        map.put(MessageType.gameInit, new TypeToken<IncomingMessage<GameInit>>() {}.getType());
        map.put(MessageType.carPositions, new TypeToken<IncomingMessage<CarPosition[]>>() {}.getType());
        map.put(MessageType.yourCar, new TypeToken<IncomingMessage<CarId>>() {}.getType());
        map.put(MessageType.gameStart, new TypeToken<IncomingMessage<NoData>>() {}.getType());
        map.put(MessageType.lapFinished, new TypeToken<IncomingMessage<LapFinished>>() {}.getType());
        map.put(MessageType.gameEnd, new TypeToken<IncomingMessage<NoData>>() {}.getType());
        map.put(MessageType.tournamentEnd, new TypeToken<IncomingMessage<NoData>>() {}.getType());
        map.put(MessageType.spawn, new TypeToken<IncomingMessage<CarId>>() {}.getType());
        map.put(MessageType.crash, new TypeToken<IncomingMessage<CarId>>() {}.getType());
        map.put(MessageType.finish, new TypeToken<IncomingMessage<NoData>>() {}.getType());
        map.put(MessageType.turboAvailable, new TypeToken<IncomingMessage<TurboAvailable>>() {}.getType());
        map.put(MessageType.createRace, new TypeToken<IncomingMessage<RaceData>>() {}.getType());
        map.put(MessageType.joinRace, new TypeToken<IncomingMessage<RaceData>>() {}.getType());
        
        msgTypeToObjectRepresentation = Collections.unmodifiableMap(map);
    }

    public IncomingMessageParser() {

    }

    public IncomingMessage<?> parse(final String message) {
        final IncomingMessagePreview messagePreview = gson.fromJson(message, IncomingMessagePreview.class);
        final MessageType objectType = messagePreview.getMsgType();
        if (objectType == null) {
            throw new UnknownMessageTypeException(message, messagePreview.getMsgType());
        }
        return gson.fromJson(message, msgTypeToObjectRepresentation.get(objectType));
    }
}
