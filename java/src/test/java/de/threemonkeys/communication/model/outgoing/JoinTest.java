package de.threemonkeys.communication.model.outgoing;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class JoinTest {
	
    @Test
    public void toJson() {
        assertThat("{\"msgType\":\"join\",\"data\":{\"key\":\"key\",\"name\":\"name\"}}",
                is(equalTo(new Join("name", "key").toJson())));
    }
    
}
