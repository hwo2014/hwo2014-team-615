package de.threemonkeys.communication.model.outgoing;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;

public class PingTest {
	
    @Test
    public void toJson() {
        assertThat("{\"msgType\":\"ping\"}", is(equalTo((new Ping()).toJson())));
    }
    
}
