package de.threemonkeys.communication.model.outgoing;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;

public class ThrottleTest {

    @Test
    public void toJson() {
        assertThat("{\"msgType\":\"throttle\",\"data\":0.75}", is(equalTo(new Throttle(0.75).toJson())));
    }
	
}
