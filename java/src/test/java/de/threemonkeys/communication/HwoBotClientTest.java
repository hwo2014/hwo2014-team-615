package de.threemonkeys.communication;

import de.threemonkeys.communication.model.outgoing.Ping;
import de.threemonkeys.communication.parser.IncomingMessageParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HwoBotClientTest {
    public static final String PING_MESSAGE = "{\"msgType\":\"ping\"}";
    public static final String JOIN_MESSAGE = "{\"msgType\":\"join\",\"data\":{\"key\":\"key\",\"name\":\"name\"}}";

    @Mock
    private BotCommunicator botCommunicator;

    @Mock
    private IncomingMessageParser incomingMessageParser;

    private HwoBotClient hwoBotClient;

    @Before
    public void init() {
        hwoBotClient = new HwoBotClient(botCommunicator, (m) -> null, incomingMessageParser);
    }

    @Test
    public void joinGameIfClientHasBeenStarted() throws IOException {
        hwoBotClient.startDefaultGame("name", "key");
        verify(botCommunicator).sendMessage(JOIN_MESSAGE);
    }

    @Test
    public void clientHandleServerMessagesTillNullMessage() throws IOException {
        when(botCommunicator.readNextMessage()).thenReturn("join", "gameInit", null);
        hwoBotClient.startDefaultGame("name", "key");
        verify(botCommunicator, times(3)).readNextMessage();
    }

    @Test
    public void clientSendResponseToServerIfMessageHandlerReturnsOutgoingMessage() throws IOException {
        hwoBotClient = new HwoBotClient(botCommunicator, (m) -> new Ping(), incomingMessageParser);
        when(botCommunicator.readNextMessage()).thenReturn("join", (String[]) null);
        hwoBotClient.startDefaultGame("name", "key");
        verify(botCommunicator).sendMessage(JOIN_MESSAGE);
        verify(botCommunicator).sendMessage(PING_MESSAGE);
    }

    @Test
    public void clientSendNoResponseToServerIfMessageHandlerReturnsNull() throws IOException {
        when(botCommunicator.readNextMessage()).thenReturn("join", (String[]) null);
        hwoBotClient.startDefaultGame("name", "key");
        verify(botCommunicator).sendMessage(JOIN_MESSAGE);
        verify(botCommunicator, times(2)).readNextMessage();
        verifyNoMoreInteractions(botCommunicator);
    }
}
