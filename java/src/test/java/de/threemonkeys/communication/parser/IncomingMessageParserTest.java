package de.threemonkeys.communication.parser;

import org.junit.Test;

public class IncomingMessageParserTest {
    @Test
    public void testCarPositionsMessage() {
        final String message = "{ \"msgType\":\"carPositions\", \"data\":[ { \"id\":{ \"name\":\"3monkeys\", \"color\":\"red\" }, \"angle\":-5.067993404267911, \"piecePosition\":{ \"pieceIndex\":14, \"inPieceDistance\":30.088809643026245, \"lane\":{ \"startLaneIndex\":0, \"endLaneIndex\":0 }, \"lap\":0} } ], \"gameId\":\"703f5099-8716-4de7-9bd7-7d258aa19f56\", \"gameTick\":240}";

        final Object tmp = new IncomingMessageParser().parse(message);
        System.out.println(tmp);
    }
}
