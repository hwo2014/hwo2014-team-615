package de.threemonkeys.communication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BotCommunicatorTest {
    @Mock
    private BufferedReader reader;

    @Mock
    private PrintWriter writer;

    private BotCommunicator botCommunicator;

    @Before
    public void init() {
        botCommunicator = new BotCommunicator(reader, writer);
    }

    @Test
    public void sendMessage() {
        final String helloWorldMessage = "Hello World";
        botCommunicator.sendMessage(helloWorldMessage);
        verify(writer).println(helloWorldMessage);
        verify(writer).flush();
    }

    @Test
    public void readMessage() throws IOException {
        final String helloWorldMessage = "Hello World";
        when(reader.readLine()).thenReturn(helloWorldMessage);
        final String message = botCommunicator.readNextMessage();
        assertThat(message, is(equalTo(helloWorldMessage)));
    }
}
