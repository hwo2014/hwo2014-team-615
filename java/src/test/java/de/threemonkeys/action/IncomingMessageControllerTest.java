package de.threemonkeys.action;

import de.threemonkeys.communication.model.MessageType;
import de.threemonkeys.communication.model.incoming.IncomingMessage;
import de.threemonkeys.communication.model.outgoing.OutgoingMessage;
import de.threemonkeys.domain.QueuedCarCommands;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IncomingMessageControllerTest {
    public static final MessageType MOCK_ACTION_MESSAGE_TYPE = MessageType.carPositions;
    public static final MessageType NOT_REGISTERED_MESSAGE_TYPE = MessageType.crash;
    private IncomingMessageController incomingMessageController;

    @Mock
    private IncomingMessage<?> incomingMessage;

    @Mock
    private IncomingMessageAction mockAction;

    @Mock
    private IncomingMessageAction defaultAction;

    @Mock
    private OutgoingMessage outgoingMessage;

    @Mock
    private QueuedCarCommands queuedCarCommands;

    @Before
    public void init() {
        incomingMessageController = new IncomingMessageController(queuedCarCommands);
        when(mockAction.getMessageType()).thenReturn(MOCK_ACTION_MESSAGE_TYPE);
        incomingMessageController.addAction(mockAction);
    }

    @Test
    public void useRegisteredActionHandlerIfMessageTypeMatch() {
        when(incomingMessage.getMessageType()).thenReturn(MOCK_ACTION_MESSAGE_TYPE);
        when(queuedCarCommands.next()).thenReturn(outgoingMessage);
        assertThat(incomingMessageController.handle(incomingMessage), sameInstance(outgoingMessage));
    }

    @Test
    public void returnNullIfNoMessageTypeAndNoDefaultActionIsRegistered() {
        when(incomingMessage.getMessageType()).thenReturn(NOT_REGISTERED_MESSAGE_TYPE);
        when(queuedCarCommands.next()).thenReturn(outgoingMessage);
        assertThat(incomingMessageController.handle(incomingMessage), sameInstance(outgoingMessage));
    }
}
